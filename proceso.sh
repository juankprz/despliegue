#$1---Nombre directorio y/o contenedor
#$2---Puerto container
DIRECTORIO=$1
if [ -d "${DIRECTORIO}" ]; then
  cd $DIRECTORIO
  APP_CONTAINER=$DIRECTORIO PORT=$2 docker-compose  down
  cd ..
  rm -f -r $DIRECTORIO
  cp -r juan_test $DIRECTORIO
  cd $DIRECTORIO
  APP_CONTAINER=$DIRECTORIO PORT=$2 docker-compose up -d
else
  cp -r juan_test $DIRECTORIO
  cd $DIRECTORIO
  APP_CONTAINER=$DIRECTORIO PORT=$2 docker-compose up -d
fi


